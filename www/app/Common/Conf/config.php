<?php
return array(
	'DEFAULT_MODULE'     => 'Chrent',
	'TMPL_ENGINE_TYPE'   =>  'Think',
	'USER_ADMINISTRATOR' => 1,
	'MODULE_ALLOW_LIST' =>  array('Chrent'),
	'MODULE_DENY_LIST'   => array('Common', 'Home'),
	'SHOW_PAGE_TRACE'    => true,
	'DEFAULT_CHARSET'    =>  'utf-8',
	'URL_MODEL'          => 0,
	'APP_DEBUG'=>true,
	'DB_FIELD_CACHE'=>false, //字段缓存
	'HTML_CACHE_ON'=>false, 
	
    /* 数据库配置 */
    'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => '127.0.0.1', // 服务器地址
    'DB_NAME'   => 'mythink', // 数据库名
    'DB_USER'   => 'root', // 用户名
    'DB_PWD'    => '123456',  // 密码
    'DB_PORT'   => '3306', // 端口
    'DB_PREFIX' => 'cht_', // 数据库表前缀
    'DB_CHARSET'=> 'utf8', // 字符集
    'TMPL_DENY_PHP' =>  false, // 默认模板引擎是否禁用PHP原生代码
);