-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014-09-05 12:36:50
-- 服务器版本： 5.6.19
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mythink`
--

-- --------------------------------------------------------

--
-- 表的结构 `cht_config`
--

CREATE TABLE IF NOT EXISTS `cht_config` (
`id` mediumint(8) NOT NULL COMMENT '配置id',
  `pid` mediumint(8) NOT NULL COMMENT '父结点',
  `code` varchar(30) NOT NULL COMMENT '配置代码',
  `code_name` varchar(60) DEFAULT NULL COMMENT '配置名称',
  `range` varchar(255) NOT NULL COMMENT '取值范围',
  `code_value` varchar(1024) DEFAULT NULL COMMENT '配置值',
  `code_remark` varchar(120) DEFAULT NULL COMMENT '配置说明',
  `code_type` varchar(20) DEFAULT NULL COMMENT '配置类型',
  `sort_order` tinyint(3) DEFAULT NULL COMMENT '排序'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='系统配置' AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `cht_config`
--

INSERT INTO `cht_config` (`id`, `pid`, `code`, `code_name`, `range`, `code_value`, `code_remark`, `code_type`, `sort_order`) VALUES
(1, 0, 'sms', '短信设置', '', NULL, NULL, 'group', 10),
(2, 0, 'mail', '邮件设置', '', NULL, NULL, 'group', 10),
(3, 1, 'sms_uid', '用户名', '', 'chrent', '', 'text', 10),
(4, 1, 'sms_pwd', '密码', '', 'cht112', '', 'password', 10),
(5, 2, 'smtp_host', 'SMTP服务器地址', '', 'smtp.chrent.com', '', 'text', 10),
(6, 2, 'smtp_port', '服务器端口', '', '25', '', 'text', 10),
(7, 2, 'smtp_user', 'SMTP用户名', '', 'lidan@chrent.com', '', 'text', 10),
(8, 2, 'smtp_pwd', 'SMTP密码', '', 'cht112', '', 'password', 10),
(9, 2, 'reply_email', '邮件回复地址', '', 'service@chrent.com', '', 'text', 10),
(10, 2, 'mail_charset', '邮件编码', 'utf8,gbk', 'utf8', '', 'select', 10);

-- --------------------------------------------------------

--
-- 表的结构 `cht_menu`
--

CREATE TABLE IF NOT EXISTS `cht_menu` (
`mid` mediumint(8) NOT NULL COMMENT '菜单id',
  `pid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '上级菜单id',
  `menu_name` varchar(60) NOT NULL COMMENT '菜单名称',
  `url` varchar(255) NOT NULL COMMENT '菜单地址',
  `ico` varchar(30) DEFAULT NULL COMMENT '菜单图标地址',
  `position` tinyint(1) DEFAULT '1' COMMENT '位置',
  `sort_order` tinyint(2) DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='系统菜单' AUTO_INCREMENT=22 ;

--
-- 转存表中的数据 `cht_menu`
--

INSERT INTO `cht_menu` (`mid`, `pid`, `menu_name`, `url`, `ico`, `position`, `sort_order`) VALUES
(1, 0, '业务管理', '', '1.png', 1, 0),
(2, 0, '财务管理', '', '2.png', 1, 0),
(3, 0, '系统管理', '', '3.png', 1, 0),
(4, 3, '菜单配置', 'Chrent/Menu/index', '', 1, 1),
(18, 17, '销售订单', 'Chrent/Oder/sale', '', 1, 1),
(7, 3, '短信管理', 'Chrent/Menu/index', '', 1, 5),
(8, 3, '邮件管理', 'Chrent/Menu/index', '', 1, 4),
(9, 3, '系统配置', 'Chrent/Config/index', '', 1, 2),
(10, 3, '角色管理', 'Chrent/Role/index', '', 1, 3),
(17, 0, '订单管理', 'Chrent/Order', '4.png', 1, 1),
(19, 17, '租赁订单', 'Chrent/Rent/index', '', 1, 0),
(20, 17, '租入订单', 'Chrent/Rentin/index', '', 1, 0),
(21, 17, '采购订单', 'Chrent/Buy', '', 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `cht_role`
--

CREATE TABLE IF NOT EXISTS `cht_role` (
`id` smallint(3) NOT NULL COMMENT '角色id',
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `rules` varchar(1024) DEFAULT NULL COMMENT '功能节点id，多个规则 , 隔开',
  `sort_order` smallint(3) DEFAULT NULL COMMENT '排序'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户角色表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cht_users`
--

CREATE TABLE IF NOT EXISTS `cht_users` (
`uid` mediumint(8) NOT NULL COMMENT '会员id',
  `user_name` varchar(60) DEFAULT NULL COMMENT '用户名',
  `real_name` varchar(60) DEFAULT NULL COMMENT '真实姓名',
  `nickname` varchar(60) DEFAULT NULL COMMENT '昵称',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `question` varchar(255) DEFAULT NULL COMMENT '密码提问',
  `answer` varchar(255) DEFAULT NULL COMMENT '密码回答',
  `sex` tinyint(1) DEFAULT NULL COMMENT '姓别0保密，1男，2女',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `user_money` decimal(10,2) DEFAULT NULL COMMENT '用户现有资金',
  `frozen_money` decimal(10,2) DEFAULT NULL COMMENT '用户冻结资金',
  `address_id` mediumint(8) DEFAULT NULL COMMENT '收货信息地址id',
  `reg_ip` varchar(15) DEFAULT NULL COMMENT '注册ip',
  `reg_time` int(10) DEFAULT NULL COMMENT '注册时间',
  `last_login_time` int(10) DEFAULT NULL COMMENT '最后一次登录时间',
  `last_login_ip` varchar(15) DEFAULT NULL COMMENT '最后一次登录ip',
  `last_time` datetime DEFAULT NULL COMMENT '最后一次修改信息时间',
  `visit_count` smallint(5) DEFAULT NULL COMMENT '访问次数',
  `parent_uid` mediumint(9) DEFAULT NULL COMMENT '推荐人会员ID',
  `email` varchar(60) DEFAULT NULL COMMENT '会员email',
  `qq` varchar(20) DEFAULT NULL COMMENT 'qq账号',
  `phone` varchar(20) DEFAULT NULL COMMENT '办公电话',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `role` varchar(60) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='网站用户表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `cht_users`
--

INSERT INTO `cht_users` (`uid`, `user_name`, `real_name`, `nickname`, `password`, `question`, `answer`, `sex`, `birthday`, `user_money`, `frozen_money`, `address_id`, `reg_ip`, `reg_time`, `last_login_time`, `last_login_ip`, `last_time`, `visit_count`, `parent_uid`, `email`, `qq`, `phone`, `mobile`, `status`, `role`) VALUES
(1, 'admin', '卓战友', '害虫', '48209bdc2261c2ca501541bfd998d179', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1409888832, '2130706433', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(2, 'zhanyou', '卓战友', '你懂的', '3f6fd6aa14d4a06f6f717827fc657e60', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1408523276, '2130706433', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(3, 'lidan', '李丹', 'lidan', '14a44b233dd71f8dfad8ed134c0f07f3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1408496178, '2130706433', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cht_config`
--
ALTER TABLE `cht_config`
 ADD PRIMARY KEY (`id`), ADD KEY `pid` (`pid`), ADD KEY `code` (`code`);

--
-- Indexes for table `cht_menu`
--
ALTER TABLE `cht_menu`
 ADD PRIMARY KEY (`mid`), ADD KEY `pid` (`pid`), ADD KEY `position` (`position`);

--
-- Indexes for table `cht_role`
--
ALTER TABLE `cht_role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cht_users`
--
ALTER TABLE `cht_users`
 ADD PRIMARY KEY (`uid`), ADD KEY `email` (`email`), ADD KEY `parent_id` (`parent_uid`), ADD KEY `user_name` (`user_name`), ADD KEY `mobile` (`mobile`), ADD KEY `status` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cht_config`
--
ALTER TABLE `cht_config`
MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '配置id',AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cht_menu`
--
ALTER TABLE `cht_menu`
MODIFY `mid` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '菜单id',AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `cht_role`
--
ALTER TABLE `cht_role`
MODIFY `id` smallint(3) NOT NULL AUTO_INCREMENT COMMENT '角色id';
--
-- AUTO_INCREMENT for table `cht_users`
--
ALTER TABLE `cht_users`
MODIFY `uid` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '会员id',AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
